Event: 95934386-b4c5-11e6-b8a8-f75c5e4e545c

Ref: https://www.washingtonpost.com/news/acts-of-faith/wp/2016/11/23/the-racist-roots-of-white-evangelicalism-and-the-rise-of-donald-trump/
 
Format:
- about 1 hr
- each prepare a prayer target, a testimony sharing (10 mins) based on the Ref above, and biblical reference to the sharing
- language: in the case of multilanguage, the least spoken language in the group
- start with testimony sharing round robin, and end with prayers for the prepared targets
- there will be no discussions and no answers to any questions

